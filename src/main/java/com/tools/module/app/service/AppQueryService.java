package com.tools.module.app.service;


import com.tools.common.model.Result;

public interface AppQueryService {

    Result crud(String sql);
}
